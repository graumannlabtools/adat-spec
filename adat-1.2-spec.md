# ADAT 1.2.0 Unofficial File Specification

## Status

The document is based upon the official SomaLogic ADAT 1.2.0 file spec.  That document is not publically available, and misses some important information. There are still some unknown details, but the format is mostly well described.

## Background

This document describes the format for ADAT files (version `1.2.0`), as created by [SomaLogic](http://www.somalogic.com/Homepage.aspx)'s [SOMAscan](http://www.somalogic.com/Technology/SOMAscan-basic-info.aspx) assay platform.

Note that there is a tension within this document because the ADAT format is general enough to contain the data from a variety of biological experiments, particularly assay experiments.  The generalized version of the spec has very few (no?) compulsory fields.  However, since the file format was created by SomaLogic, and is used almost exclusively for their experiments, there are many fields which should always appear in the files.

## File Format

- ADAT files are ASCII or [WE8ISO8859P1](https://docs.oracle.com/cd/A87860_01/doc/server.817/a76966/ch3.htm) encoded, tab-delimited text files.
- The line ending is CR/LF.
- The file extension is `adat`.

## Field Names

- Field names must be unique within the file.  That is, they cannot be duplicated, even between the header/sequence data/sample data sections.
- Field names are limited to 64 characters, and cannot contain blanks or tabs.
- Field names are case sensitive.

## Field Data

- Field data values can contain up to 4000 characters, and cannot contain tabs.

## Date, File Path, and Numeric Version Formats

- Dates should be in [ISO 8601](http://www.iso.org/iso/iso8601) format, `yyyy-mm-dd`.  For example, `1999-12-31`.
- File paths should be given as absolute paths, and be separated by `/`, even on Windows.  For example `c:/data/file1.adat`.
- Numeric versions should consists of two or three natural numbers separated by periods.  For example, `1.23` or `1.23.456`.

## File Contents

ADAT files consists of of six sections: checksum, header data, column data specification, row data specification, table beginning, sequence data, and sample data and intensities.

### Checksum

- The first line of the file should contain two fields.
- The first field of this line should contain the value `!Checksum`.
- The second field of this line should contain a [SHA-1](http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.180-4.pdf) checksum of the remaining lines of the file.

### Header Data

- The second line of the file should contain a single field, containing the value `^HEADER`.
- Further lines should contain two fields.
- Values in the first field should begin with an exclamation mark, `!`, followed by the name of the header data item.
- Values in the second field should contain the value of the header data item.
- Data values containing multiple items should be separated by a command then a space, except where noted.  For example `value1, value2`.
- The header section continues until the start of the column data section begins.
- There are no compulsory header fields.

Common header fields: 

- **Version**: *Numeric version*. The ADAT file spec number, in this case always `1.2`.
- **AssayType**: *String*. This describes the assay experiment type.  For example, `GLP-Like Assay Experiment`.
- **AssayRobot**: *String*. This describes the make and model number of the assay workstation. For example, `Beckman BioMek Fx`.
- **AssayVersion**: `v` followed by a *Numeric Version*.  The version number of the SOMAscan assay protocol. For example, `v3.1`.
- **CreatedBy**: *String*.  This describes the person or team that created by the dataset.  For example, `PharmaServices`.
- **CreatedDate**: *Date*. The date that the file was created.
- **DerivedFrom**: *Strings*. Absolute paths to any files that this file was constructed from.  This may occur when samples from multiple experiments are combined into a single file.  In this case, the field may contain paths to ADAT files for the individual experiments.  For example `c:/Data/Experiment1.adat, c:/Data/Experiment2.adat`. 
- **EnteredBy**: *String*.  The name of the person who created the file.
- **ExpDate**: *Dates*.  The dates that the experiments were conducted on.  For example, `2015-01-01, 2015-01-03`.  **TODO: Does the order of the dates mean anything?  They aren't currently always in chronological order.**
- **ExpIds**: *String*. Identifiers for each experiment, provided by the customer.  **TODO: Is this correct?** 
- **GeneratedBy**: *String*. `Px` **TODO: What is 'Px'?  What other values are allowed here?** followed by a space, followed by the build number and corresponding SHA-1 checksum in parentheses.  For example, `Px (Build: 736 : 0efbddf53702efab3533738f5cce50047cdb7227)`.
- **MasterMixVersion**: `v` followed by a *Numeric Version*, followed by  either `Plasma` or `Serum`.  The version of the PCR reagent master mix used in the SOMAmer formulation.
- **SomascanMenuVersion**. **String** The version of the list of reported SOMAmers.  The 1310 assay panel is known as `V1.3k`, and the 1129 assay panel is known as `V1.1k` or `V3.2`.
- For each Plate, **PlateMedianCal\_*PlateId***, where *PlateId* is one of the values in the PlateId field of the sample data (see 'Sample Data' section), with spaces replaced by underscores: *Numeric*.  The median calibration value for that plate.  **TODO: How is this calculated?**
- **ProcessSteps**: *Strings*. Text description of steps in the data processing, or links to documents that describe these steps.  Individual steps should be separated by a semi-colon or comma then a space.  For example, `Raw RFU, Hyb Normalization, Median Normalization, Calibration, Filtered`.
- **ProteinEffectiveDate**: *Date*.  The date that the protein was **TODO: What is this?**
- **ReportType**: *String*.  The status of the file.  By the time it reaches a customer, it should have the value `Prepared for Release`.
- **StudyMatrix**: *String*.  The matrix used in the experiments.  For example, `EDTA Plasma`.
- **StudyOrganism**: *String*. The name of the organism that the samples relate to.  For example, `Human` or `Mouse`.
- **Title**: *String*.  The name of the experiment. For example `Jenner Smallpox 123`.

### Column Data Specification

- This section should contain three lines.
- The first line should contain a single field, containing the value `^COL_DATA`.
- The first field in the second line should contain the value `!Name`.
- The rest of the fields in the second line should contain the names of the fields used in the `Sequence Data` section of the file.
- The third line should contain as many fields as the second line.
- The first field in the third line should contain the value `!Type`.
- The rest of the fields in the third line should contain the value `String`. Previously this was designed to hold the type of each variable, but this feature has been deprecated.

The number of sequence fields (beyond `!Name`/`!Type`) is henceforth referred to as `N_SEQ_FIELDS`.

Compulsory column header fields:

- **SeqId**: *String*.  A unique identifer for the SOMAmer reagent.
- **Target**: *String*.  The unique name for the targeted proteins.

These columns are not compulsory for the file spec, but are always included by SomaLogic:

- **SomaId**: *String*.  The SomaLogic identifier for the protein target. For the 1129 and 1310 assays, there is a one-to-one correspondence between SeqId and SomaId, but in theory there is a many-to-one correspondence.
- **TargetFullName**: *String*.  A description of the proteins targeted by the SOMAmer, taken from UniProt.
- **UniProt**: *String*.  The UniProt identifiers for the targeted proteins.  Older versions of the file format may also contain the value 'Family', referring to a whole family of proteins, though this behaviour is considered deprecated.
- **EntrezGeneID**: *String*.  The Entrez Gene identifiers for the genes corresponding to the targeted proteins.
- **EntrezGeneSymbol**: *String*.  The Entrez Gene symbols for the genes corresponding to the targeted proteins.
- **Organism**: *String*.  What animal does the target protein refer to?
- **ColCheck**: *String*.  Either `PASS` or `FAIL`. Did the sequence pass quality control checks?
- **Units**: *String*.  Unit of measurement for the SOMAmer abundances.  Should always be `RFU`.
- **Type**: *String*.  One of `Spuriomer`, `Protein`, `Hybridization Control`, `Non-human Protein`.
- **Cal\__PlateId_**: *Number*.  Calibration scale factor for each plate.  For example, `1.23`.
- **CalReference**: *Number*.  Calibration reference intensity, in RFU.  For example, `543.21`.  **TODO: Is this correct?**
- **Dilution**: *Number*.  Concentration of the diluted sample compared to the neat sample, as a percentage. Either 40, 1, or 0.005.

### Row Data Specification

- This section should contain three lines.
- The first line should contain a single field, containing the value `^ROW_DATA`.
- The first field in the second line should contain the value `!Name`.
- The rest of the fields in the second line should contain the names of the fields used in the 'Sample Data' section of the file.
- The third line should contain as many fields as the second line.
- The first field in the third line should contain the value `!Type`.
- The rest of the fields in the third line should contain the value `String`. Previously this was designed to hold the type of each variable, but this feature has been deprecated.

The number of sample fields (beyond `!Name`/`!Type`) is henceforth referred to as `N_SAMPLE_FIELDS`.

### Table Beginning

- The next line should should contain a single field, containing the value `^TABLE_BEGIN`.

### Sequence Data

The number of SOMAmers in the SOMAscan assay is henceforth referred to as `N_SOMAMERS`.

- This section should contain `N_SEQ_FIELDS` lines.
- Each line should contain `N_SAMPLE_FIELDS + 1 + N_SOMAMERS` fields.
- In each line, the first `N_SAMPLE_FIELDS` should be blank.
- In each line, the `N_SAMPLE_FIELDS + 1`th field should contain the field names for the sequence data.  That is, the same values that are contained in the second line of the 'Column Data Specification' section.
- In each line, the `N_SAMPLE_FIELDS + 2`th to `N_SAMPLE_FIELDS + 1 + N_SOMAMERS`th fields should contain the value of the sequence data item for a given SOMAmer.
- For fields with multiple values (for example, UniProt IDs), each value should be separated by a comma or space, and optional additional spaces.  **TODO: This is pretty awful.  May need to specify this more rigorously in future versions.**

### Sample Data And Intensities

- This section should contain `N_SAMPLE_FIELDS + 1` lines.
- Each line should contain `N_SAMPLE_FIELDS + 1 + N_SOMAMERS` fields.
- The first line should contain the field names for the sample data.  That is, the same values that are contained in the second line of the 'Row Data Specification' section.
- In remaining lines, the first `N_SAMPLE_FIELDS` should contain the value of the sample data item for a given sample.
- In remaining lines, the `N_SAMPLE_FIELDS + 1`th field should be blank.
- In remaining lines, the `N_SAMPLE_FIELDS + 2`th to `N_SAMPLE_FIELDS + 1 + N_SOMAMERS`th fields should contain the intensity for a given sample and SOMAmer.

Compulsory row header fields:

- **ExternalId**: *String*.  A unique identifer for the sample.

These columns are not compulsory for the file spec, but are always included by SomaLogic:

- **SampleId**: *String*.  The customer's original sample identifier.   This is either the same as the subject identifier, or the calibrator and buffer separated by a hyphen.  For example `1234` or `PPT-09`.
- **TimePoint**: *String* or *Number*.  The time point identified by the customer.
- **SampleGroup**: *String*.  Cohort information provided by the customer.
- **SampleNotes**: *String*.  Lab comments about the sample condition.
- **AssayNotes**: *String*.  Lab comments about assay adverse events.

Common optional column header fields:

- **PlateId**. *String* identifier for the plate.  For assay experiments, this also functions as an experiment identifier.
- **SlideId**. *String*. Agilent slide barcode.
- **Subarray**. *Integer*, from `1` to `8`. Agilent subarray number.
- **SampleType**. *String*. Either `Sample`, `QC`, `Buffer` or `Calibrator`.
- **BarCode**. *String*. 1D Barcode of aliquot. **TODO: What's an aliquot?**
- **Barcode2d**. *String*. 2D Barcode of aliquot.  **TODO: Is this actually related to the aliquot?**
- **SiteId**. *String*. The laboratory that ran the experiment, applicable if subcontracted.
- **SampleUniqueId**: *String*.  Where multiple experiments are combined, this column can be used to ensure a  unique sample ID.
- **Subject_ID**. *String*. Identifier for the person/animal/thing being sampled.
- **HybControlNormScale**. *Number*. Hybridization control normalization scale factor.  For example, `1.23`.
- **NormScale_40**. *Number*. Median normalization scale factor, for only the sequences diluted to 40% concentration.  For example, `1.23`.
- **NormScale_0_005**. *Number*. Median normalization scale factor, for only the sequences diluted to 0.005% concentration.  For example, `1.23`.
- **NormScale_1**. *Number* . Median normalization scale factor, for only the sequences diluted to 1% concentration.  For example, `1.23`.
- **PercentDilution**. *Number* How much was the Either 40, 1, or 0.005.
- **SampleMatrix**. *String* As per the SampleMatrix header field.  Applicable if different matrices are used with different samples in the file.
- **SampleDescription**. *String* Free text describing the sample.
- **TubeUniqueID**. *String* A unique identifier for every sample tube, assigned by the customer.
- **RowCheck**. *String* Either `PASS` or `FAIL`. Did the sample pass quality control checks?

Allowed intensity values:

Intensities should be a number where possible, but can also take the following non-numeric values.

- **HIGH**. A value outside the linear range of values, above the plateau.  
- **LOW**. A value outside the linear range of values, below the baseline.  
- **>n.nnn**. A value above the upper limit of quantification.  
- **<n.nnn**. A value below the lower limit of quantification.  
